# Dots Experimentations

## For all projects

The projects already have their dependencies and framework attach to them. All it needs is to open the projects in Android Studio or Xcode and build them.

## For iOS projects

A Mac and Xcode is needed to run these apps.

## For dots server

To start the php server, go to dots-server directory

````bash
$ cd dots-server
```

and write this command:

````bash
$ php artisan serve
```

To open a web chatroom, go to localhost on the port prompted and go to /chatroom.
