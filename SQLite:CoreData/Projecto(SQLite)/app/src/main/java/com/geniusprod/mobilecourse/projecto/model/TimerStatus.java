package com.geniusprod.mobilecourse.projecto.model;

/**
 * Created by benoitlevesque on 2016-10-14.
 *
 */

public enum TimerStatus {
    RUNNING, NOT_RUNNING
}
