package com.geniusprod.mobilecourse.projecto.model.repository;

import android.provider.BaseColumns;

/**
 * Created by benoitlevesque on 2016-10-14.
 *
 */

public final class ProjectsContract {

    private ProjectsContract() { }

    public static class ProjectEntry implements BaseColumns {
        public static final String TABLE_NAME = "Projects";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_SECONDS = "seconds";
        public static final String COLUMN_NAME_TIMER_STATUS = "timerStatus";
    }

}
