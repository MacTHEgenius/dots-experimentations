package com.geniusprod.mobilecourse.projecto.model.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.geniusprod.mobilecourse.projecto.model.Project;
import com.geniusprod.mobilecourse.projecto.model.TimerStatus;
import com.geniusprod.mobilecourse.projecto.model.repository.Exceptions.ProjectNotFoundException;
import com.geniusprod.mobilecourse.projecto.model.repository.ProjectsContract;
import com.geniusprod.mobilecourse.projecto.model.repository.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by benoitlevesque on 2016-10-14.
 *
 */
public class ProjectsService extends Repository<Project> {

    private static Repository<Project> instance;
    public static Repository<Project> getInstance(Context context) {
        if (instance == null) {
            instance = new ProjectsService(context);
        }
        return instance;
    }

    private static final int DEFAULT_TIMER_VALUE = 0;

    private ProjectsService(Context context) {
        super(context);
    }

    public void insertAll(List<Project> projects) {
        for (Project project : projects) {
            insert(project);
        }
    }

    public long insert(Project project) {
        ContentValues values = new ContentValues();
        values.put(ProjectsContract.ProjectEntry.COLUMN_NAME_NAME, project.name);
        values.put(ProjectsContract.ProjectEntry.COLUMN_NAME_SECONDS, project.seconds);
        values.put(ProjectsContract.ProjectEntry.COLUMN_NAME_TIMER_STATUS, String.valueOf(project.timerStatus));
        return mWriteDB.insert(ProjectsContract.ProjectEntry.TABLE_NAME, null, values);
    }

    public List<Project> getAll() {
        List<Project> projects = new ArrayList<>();
        Cursor cursor = mReadDB.rawQuery("select * from " + ProjectsContract.ProjectEntry.TABLE_NAME, null);

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ProjectsContract.ProjectEntry._ID));
                String name = cursor.getString(cursor.getColumnIndex(ProjectsContract.ProjectEntry.COLUMN_NAME_NAME));
                int seconds = cursor.getInt(cursor.getColumnIndex(ProjectsContract.ProjectEntry.COLUMN_NAME_SECONDS));
                TimerStatus timerStatus = TimerStatus.valueOf(cursor.getString(cursor.getColumnIndex(ProjectsContract.ProjectEntry.COLUMN_NAME_TIMER_STATUS)));
                Project newProject = new Project(id, name, seconds, timerStatus);
                projects.add(newProject);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return projects;
    }

    public Project getById(int id) throws ProjectNotFoundException {
        String[] projection = {
                ProjectsContract.ProjectEntry._ID,
                ProjectsContract.ProjectEntry.COLUMN_NAME_NAME,
                ProjectsContract.ProjectEntry.COLUMN_NAME_SECONDS,
                ProjectsContract.ProjectEntry.COLUMN_NAME_TIMER_STATUS
        };
        String selection = ProjectsContract.ProjectEntry._ID + " = ?";
        String[] selectionArgs = { Integer.toString(id) };

        Cursor cursor = mReadDB.query(ProjectsContract.ProjectEntry.TABLE_NAME,
                projection, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) {
            String name = cursor.getString(cursor.getColumnIndex(ProjectsContract.ProjectEntry.COLUMN_NAME_NAME));
            int seconds = cursor.getInt(cursor.getColumnIndex(ProjectsContract.ProjectEntry.COLUMN_NAME_SECONDS));
            TimerStatus timerStatus = TimerStatus.valueOf(cursor.getString(cursor.getColumnIndex(ProjectsContract.ProjectEntry.COLUMN_NAME_TIMER_STATUS)));
            cursor.close();
            return new Project(id, name, seconds, timerStatus);
        } else {
            throw new ProjectNotFoundException("Project with id <" + id + "> was not found.");
        }
    }

    public void deleteAll() {
        mWriteDB.delete(ProjectsContract.ProjectEntry.TABLE_NAME, null, null);
    }

    public void delete(Project project) {
        String selection = ProjectsContract.ProjectEntry._ID + " like ?";
        String[] selectionArgs = { Integer.toString(project.id) };
        mWriteDB.delete(ProjectsContract.ProjectEntry.TABLE_NAME, selection, selectionArgs);
    }

    public void updateAll(List<Project> projects) {
        for (Project project : projects) {
            update(project);
        }
    }

    public void update(Project project) {
        ContentValues values = new ContentValues();
        values.put(ProjectsContract.ProjectEntry.COLUMN_NAME_NAME, project.name);
        values.put(ProjectsContract.ProjectEntry.COLUMN_NAME_SECONDS, project.seconds);
        values.put(ProjectsContract.ProjectEntry.COLUMN_NAME_TIMER_STATUS, String.valueOf(project.timerStatus));

        String selection = ProjectsContract.ProjectEntry._ID + " like ?";
        String[] selectionArgs = { Integer.toString(project.id) };
        mReadDB.update(ProjectsContract.ProjectEntry.TABLE_NAME, values, selection, selectionArgs);
    }

}
