package com.geniusprod.mobilecourse.projecto.model.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

/**
 * Created by benoitlevesque on 2016-10-15.
 *
 */

public abstract class Repository<T> {

    protected SQLiteDatabase mWriteDB;
    protected SQLiteDatabase mReadDB;

    public Repository(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        mReadDB = dbHelper.getReadableDatabase();
        mWriteDB = dbHelper.getWritableDatabase();
    }

    public abstract void insertAll(List<T> list);

    public abstract long insert(T e);

    public abstract List<T> getAll();

    public abstract T getById(int id);

    public abstract void deleteAll();

    public abstract void delete(T e);

    public abstract void updateAll(List<T> list);

    public abstract void update(T e);
}
