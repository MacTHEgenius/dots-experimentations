package com.geniusprod.mobilecourse.projecto.model;

/**
 * Created by benoitlevesque on 2016-10-14.
 *
 */

public class Project {

    public static final int INIT_TIMER = 0;
    public static final TimerStatus DEFAULT_TIME_STATUS = TimerStatus.NOT_RUNNING;

    public int id;
    public final String name;
    public final int seconds;
    public final TimerStatus timerStatus;

    public Project(String name, int seconds, TimerStatus timerStatus) {
        this.name = name;
        this.seconds = seconds;
        this.timerStatus = timerStatus;
    }

    public Project(String name) {
        this(name, INIT_TIMER, DEFAULT_TIME_STATUS);
    }

    public Project(int id, String name, int seconds, TimerStatus timerStatus) {
        this(name, seconds, timerStatus);
        this.id = id;
    }

    @Override
    public String toString() {
        return "<" + this.id + ", " + this.name + ", " + this.seconds + ", " + this.timerStatus + ">";
    }
}
