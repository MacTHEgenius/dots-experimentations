package com.geniusprod.mobilecourse.projecto.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.geniusprod.mobilecourse.projecto.R;
import com.geniusprod.mobilecourse.projecto.model.Project;
import com.geniusprod.mobilecourse.projecto.model.repository.Repository;
import com.geniusprod.mobilecourse.projecto.model.services.ProjectsService;

import java.util.List;

public class ProjectsActivity extends Activity {

    public final String TAG = "PAct";

    private EditText mNewProjectEditText;
    private Button mAddProjectButton;
    private Button mResetTimersButton;
    private ListView mProjectsListView;

    private Repository<Project> mProjectsService;
    private List<Project> projects;

    private ArrayAdapter<Project> projectsAdapter;

    //region On click methods

    public void addProjectAction(View view) {
        String newProjectName = mNewProjectEditText.getText().toString();
        Toast toast = Toast.makeText(this, R.string.toast_project_name_empty, Toast.LENGTH_LONG);

        if (!newProjectName.equals("")) {
            Project project = new Project(newProjectName);
            mProjectsService.insert(project);
//            this.projects.add(project);
            toast.setText(R.string.toast_project_inserted);
            this.updateListViewData();
        }

        toast.show();
    }

    public void resetTimersAction(View view) {
        Log.d(TAG, "Reset");
    }

    //endregion

    //region Activity methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);

        this.registerServices();

//        this.insertTestData();
        this.loadProjects();

        this.findViewsById();
        this.registerAdapter();
        this.updateListViewData();
    }

    //endregion

    private void registerServices() {
        mProjectsService = ProjectsService.getInstance(this);
    }

    private void findViewsById() {
        mNewProjectEditText = (EditText) findViewById(R.id.new_project_name_edit_text);
        mAddProjectButton = (Button) findViewById(R.id.add_project_button);
        mResetTimersButton = (Button) findViewById(R.id.reset_all_timers_button);
        mProjectsListView = (ListView) findViewById(R.id.listView);
    }

    private void registerAdapter() {
        this.projectsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mProjectsService.getAll());
        mProjectsListView.setAdapter(this.projectsAdapter);
    }

    private void loadProjects() {
        this.projects = mProjectsService.getAll();
        Log.d(TAG, this.projects.toString());
    }

    private void updateListViewData() {
        this.projects = mProjectsService.getAll();
        Log.d(TAG, this.projects.toString());
        this.projectsAdapter.notifyDataSetChanged();
    }

    // Other

    private void insertTestData() {
        mProjectsService.deleteAll();
        Project newProject = new Project("Swift");
        Project newProject2 = new Project("Java");
        mProjectsService.insert(newProject);
        mProjectsService.insert(newProject2);
    }
}
