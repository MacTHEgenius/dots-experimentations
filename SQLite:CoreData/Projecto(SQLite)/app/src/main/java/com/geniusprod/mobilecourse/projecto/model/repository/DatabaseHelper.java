package com.geniusprod.mobilecourse.projecto.model.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by benoitlevesque on 2016-10-14.
 *
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "Projects.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ProjectsContract.ProjectEntry.TABLE_NAME + " (" +
                    ProjectsContract.ProjectEntry._ID + " INTEGER PRIMARY KEY autoincrement," +
                    ProjectsContract.ProjectEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    ProjectsContract.ProjectEntry.COLUMN_NAME_SECONDS + INTEGER_TYPE + COMMA_SEP +
                    ProjectsContract.ProjectEntry.COLUMN_NAME_TIMER_STATUS + TEXT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + ProjectsContract.ProjectEntry.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
