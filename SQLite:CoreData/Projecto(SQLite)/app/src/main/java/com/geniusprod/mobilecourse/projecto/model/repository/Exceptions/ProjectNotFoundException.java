package com.geniusprod.mobilecourse.projecto.model.repository.Exceptions;

/**
 * Created by benoitlevesque on 2016-10-14.
 *
 */

public class ProjectNotFoundException extends RuntimeException {

    public ProjectNotFoundException(String message) {
        super(message);
    }

}
