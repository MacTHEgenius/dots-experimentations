//
//  ViewController.swift
//  What is the Weather
//
//  Created by Benoît Lévesque on 16-01-03.
//  Copyright © 2016 GeniusProd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var cityTextField: UITextField!
    
    @IBOutlet var answerLabel: UILabel!
    
    @IBAction func findOut(sender: AnyObject) {
        var wasSuccessful = false
        if let url = NSURL(string: "http://www.weather-forecast.com/locations/" + cityTextField.text!.stringByReplacingOccurrencesOfString(" ", withString: "-") + "/forecasts/latest") {
            
            let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) -> Void in
                if let urlContent = data {
                    let webContent = NSString(data: urlContent, encoding: NSUTF8StringEncoding)
                    let websiteArray = webContent!.componentsSeparatedByString("3 Day Weather Forecast Summary:</b><span class=\"read-more-small\"><span class=\"read-more-content\"> <span class=\"phrase\">")
                    
                    if websiteArray.count > 1 {
                        
                        let weatherArray = websiteArray[1].componentsSeparatedByString("</span>")
                        if weatherArray.count > 1 {
                            
                            wasSuccessful = true
                            let weatherSummary = weatherArray[0].stringByReplacingOccurrencesOfString("&deg;", withString: "°")
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.answerLabel.text = weatherSummary
                            })
                            
                        }
                        
                    }
                    
                }
                
                if !wasSuccessful {
                    self.answerLabel.text = "Couldn't find the weather for that city. Try again."
                }
                
            }
            task.resume()
        } else {
            self.answerLabel.text = "Couldn't find the weather for that city. Try again."
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

